<?php
$num = 0;

while ($num < 20)
{
    $num++;
    if ($num == 15) {
        continue;
    } else {
        echo "Continue at 15 (".$num.").<br />";
    }

}

$age = 15;
while($age<30){
    $age++;
    if($age == 20){
        continue;
    }else{
        echo "Your age is ".$age." years old <br/>";
    }
}

$fruits = 0;
while($fruits<9){
    $fruits++;
    if($fruits>5){
        continue;
    }else{
        echo 'This is '.$fruits.' no fruit <br/>';
    }
}